package fr.webscraping.access;


import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.List;


import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.jboss.logging.Logger;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import fr.webscraping.access.client.IStockageService;
import fr.webscraping.access.client.JobAdvert;

@Path("/api/access")
@Produces(APPLICATION_JSON)
public class DataAccess {
    private static final Logger LOGGER = Logger.getLogger(DataAccess.class);

    @Inject
    @RestClient
    IStockageService stockageService;

    @Operation(summary = "Returns all job adverts")
    @APIResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON, schema = @Schema(implementation = JobAdvert.class, type = SchemaType.ARRAY)))
    @GET
    public Response getAllJobs() {
        return stockageService.getAllJobs();
    }

    @Operation(summary = "Finds a job by id")
    @APIResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON, schema = @Schema(implementation = JobAdvert.class, required = true)))
    @GET
    @Path("/{id}")
    public Response findJobById(@PathParam("id") final Long id) {
        return stockageService.getJobById(id);
    }
}