package fr.webscraping.access.client;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/stockage")
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient
public interface IStockageService {
    @GET
    public Response getAllJobs();

    @GET
    public Response getJobById(final Long id);
}
