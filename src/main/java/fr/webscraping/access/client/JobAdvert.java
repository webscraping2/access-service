package fr.webscraping.access.client;

import javax.validation.constraints.NotNull;

public class JobAdvert {
    @NotNull
    public String title;
    
    @NotNull
    public String company;

    @NotNull
    public String contract;

    @NotNull
    public String location;
    
    @NotNull
    public String date;

    public JobAdvert(final String title, final String company, final String contract, final String location, final String date) {
        this.title = title;
        this.company = company;
        this.contract = contract;
        this.location = location;
        this.date = date;
    }

    @Override
    public String toString() {
        return "JobAdvert{" +
            ", title='" + title + '\'' +
            ", company='" + company + '\'' +
            ", contract=" + contract +
            ", location='" + location + '\'' +
            ", date='" + date + '\'' +
            '}';
    }
}
