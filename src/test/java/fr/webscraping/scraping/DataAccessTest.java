package fr.webscraping.scraping;


import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.hamcrest.core.Is;
import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.*;
import static org.hamcrest.CoreMatchers.*;

@QuarkusTest
public class DataAccessTest {
    private static final String PATH = "api/stockage";
    private static final String PATH_WITH_ID = "/api/stockage/{id}";
    private static final String ID_PARAM = "id";

    @Test
    void shouldPingOpenAPI() {
        given()
            .header(ACCEPT, APPLICATION_JSON)
            .when().get("/openapi")
            .then()
            .statusCode(OK.getStatusCode());
    }

    @Test
    void shouldPingSwaggerUI() {
        given()
            .when().get("/swagger-ui")
            .then()
            .statusCode(OK.getStatusCode());
    }
}